<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group( [ 'middleware' => 'adminMvc'], function() {
    Route::get('register', 'RegisterController@index');
    Route::post('register', 'RegisterController@registerUser')->name("register");
    Route::get('manage-roles', 'RoleController@manageRoles')->name('manage-roles');
    Route::post('manage-roles', 'RoleController@changeRole')->name('manage-roles');
});

Auth::routes(['register' => false]);

Route::group( ['middleware' => 'auth' ], function() {
    Route::middleware('blockLab')->get('/form-beta', 'HomeController@index');
    Route::get('/{path?}', [
        'uses' => 'HomeController@index',
        'as' => 'home',
        'where' => ['path' => '.*']
    ]);
    Route::get('/home', 'HomeController@index')->name('home');

    // Email related routes
    Route::get('mail/send', 'MailController@send');
});
