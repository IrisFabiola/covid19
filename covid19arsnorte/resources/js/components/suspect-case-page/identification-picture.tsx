import React from "react";
import {
    TextField,
    MenuItem,
    Grid,
    createStyles,
    makeStyles,
    Typography
} from "@material-ui/core";
import { DatePicker } from "../date-picker";
import { DOCUMENT_TYPES_LABELS } from "../../constants";

const useStyles = makeStyles(() =>
    createStyles({
        textField: {
            marginRight: 8,
            flexBasis: "25%",
            flexGrow: 1
        }
    })
);

interface DocumentPickerProps {
    documentNumber: string;
    documentType: string;
    disabled?: boolean;
    onDocumentNumberChange: (documentType: string) => void;
    onDocumentTypeChange: (documentType: string) => void;
}

function DocumentPicker({
    documentNumber,
    documentType,
    disabled,
    onDocumentNumberChange,
    onDocumentTypeChange
}: DocumentPickerProps): JSX.Element {
    const classes = useStyles();

    const handleDocumentTypeChange = (event): void => {
        const documentType = event.target.value;
        onDocumentTypeChange(documentType);
    };

    const handleDocumentNumberChange = (event): void => {
        const documentNumber = event.target.value;
        onDocumentNumberChange(documentNumber);
    };

    const renderStrategies = (idTypeValue): JSX.Element => {
        const label = DOCUMENT_TYPES_LABELS[idTypeValue];
        return (
            <TextField
                className={classes.textField}
                required
                disabled={disabled}
                label={label}
                value={documentNumber}
                onChange={handleDocumentNumberChange}
            />
        );
    };

    return (
        <>
            <TextField
                className={classes.textField}
                select
                required
                disabled={disabled}
                label="Tipo Documento"
                value={documentType}
                onChange={handleDocumentTypeChange}
            >
                {Object.entries(DOCUMENT_TYPES_LABELS).map(([value, label]) => (
                    <MenuItem key={value} value={value}>
                        {label}
                    </MenuItem>
                ))}
            </TextField>
            {documentType && renderStrategies(documentType)}
        </>
    );
}

interface IdentificationPictureProps {
    name: string;
    birthDate: Date;
    gender: string;
    nationality: string;
    profession: string;
    email: string;
    residence: string;
    outsideCountryResidence: string;
    location: string;
    contact: string;
    friendContact: string;
    registration: string;
    documentNumber: string;
    documentType: string;
    disabled?: boolean;
    onNameChange?: (name: string) => void;
    onBirthDateChange?: (birthDate: Date) => void;
    onGenderChange?: (gender: string) => void;
    onNationalityChange?: (nationality: string) => void;
    onProfessionChange?: (profession: string) => void;
    onEmailChange?: (email: string) => void;
    onResidenceChange?: (residence: string) => void;
    onOutsideCountryResidenceChange?: (residence: string) => void;
    onLocationChange?: (location: string) => void;
    onContactChange?: (contact: string) => void;
    onFriendContactChange?: (friendContact: string) => void;
    onRegistrationChange?: (registration: string) => void;
    onDocumentTypeChange?: (documentType: string) => void;
    onDocumentNumberChange?: (documentNumber: string) => void;
}

function IdentificationPicture({
    name,
    birthDate,
    gender,
    nationality,
    profession,
    email,
    residence,
    outsideCountryResidence,
    location,
    contact,
    friendContact,
    registration,
    documentNumber,
    documentType,
    disabled,
    onNameChange,
    onBirthDateChange,
    onGenderChange,
    onNationalityChange,
    onProfessionChange,
    onEmailChange,
    onResidenceChange,
    onOutsideCountryResidenceChange,
    onLocationChange,
    onContactChange,
    onFriendContactChange,
    onRegistrationChange,
    onDocumentTypeChange,
    onDocumentNumberChange
}: IdentificationPictureProps): JSX.Element {
    const handleNameChange = (event): void => {
        const name = event.target.value;
        onNameChange && onNameChange(name);
    };
    const handleBirthDateChange = (date: Date): void => {
        onBirthDateChange && onBirthDateChange(date);
    };
    const handleGenderChange = (event): void => {
        onGenderChange && onGenderChange(event.target.value);
    };
    const handleNationalityChange = (event): void => {
        const nationality = event.target.value;
        onNationalityChange && onNationalityChange(nationality);
    };
    const handleProfessionChange = (event): void => {
        const profession = event.target.value;
        onProfessionChange && onProfessionChange(profession);
    };
    const handleEmailChange = (event): void => {
        const email = event.target.value;
        onEmailChange && onEmailChange(email);
    };
    const handleResidenceChange = (event): void => {
        const Residence = event.target.value;
        onResidenceChange && onResidenceChange(Residence);
    };
    const handleOutsideCountryResidenceChange = (event): void => {
        const outsideCountryResidence = event.target.value;
        onOutsideCountryResidenceChange &&
            onOutsideCountryResidenceChange(outsideCountryResidence);
    };
    const handleLocationChange = (event): void => {
        const location = event.target.value;
        onLocationChange && onLocationChange(location);
    };
    const handleContactChange = (event): void => {
        const contact = event.target.value;
        onContactChange && onContactChange(contact);
    };
    const handleFriendContactChange = (event): void => {
        const friendContact = event.target.value;
        onFriendContactChange && onFriendContactChange(friendContact);
    };
    const handleRegistrationChange = (event): void => {
        const registration = event.target.value;
        onRegistrationChange && onRegistrationChange(registration);
    };

    const classes = useStyles();

    return (
        <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            wrap="wrap"
        >
            <h2>Identificação do caso:</h2>
            <TextField
                fullWidth
                required
                disabled={disabled}
                label="Nome"
                value={name}
                onChange={handleNameChange}
            />
            <Grid item className={classes.textField}>
                <DatePicker
                    required
                    disabled={disabled}
                    label="Data de nascimento"
                    value={birthDate}
                    onDateChange={handleBirthDateChange}
                    autoOk
                    disableFuture
                />
            </Grid>
            <TextField
                className={classes.textField}
                select
                disabled={disabled}
                label="Género"
                value={gender}
                onChange={handleGenderChange}
            >
                <MenuItem value="male">Masculino</MenuItem>
                <MenuItem value="female">Feminino</MenuItem>
            </TextField>
            <DocumentPicker
                disabled={disabled}
                documentType={documentType}
                documentNumber={documentNumber}
                onDocumentTypeChange={onDocumentTypeChange}
                onDocumentNumberChange={onDocumentNumberChange}
            />
            <TextField
                label="Nacionalidade"
                disabled={disabled}
                value={nationality}
                onChange={handleNationalityChange}
            />
            <TextField
                label="Profissão"
                disabled={disabled}
                value={profession}
                onChange={handleProfessionChange}
            />
            <TextField
                label="Email"
                type="email"
                disabled={disabled}
                value={email}
                onChange={handleEmailChange}
            />
            <TextField
                required
                fullWidth
                disabled={disabled}
                label="Residência"
                value={residence}
                onChange={handleResidenceChange}
            />
            <TextField
                fullWidth
                disabled={disabled}
                label="País de residência se fora de Portugal"
                value={outsideCountryResidence}
                onChange={handleOutsideCountryResidenceChange}
            />
            <TextField
                fullWidth
                disabled={disabled}
                label="Local do Caso"
                value={location}
                onChange={handleLocationChange}
            />
            <div className="tripleInputContainer">
                <div>
                    <TextField
                        fullWidth
                        required
                        disabled={disabled}
                        label="Contacto do caso"
                        type="tel"
                        value={contact}
                        onChange={handleContactChange}
                    />
                </div>
                <div>
                    <TextField
                        fullWidth
                        disabled={disabled}
                        label="Contacto Acompanhante"
                        type="tel"
                        value={friendContact}
                        onChange={handleFriendContactChange}
                    />
                </div>
                <div>
                    <TextField
                        fullWidth
                        required
                        disabled={disabled}
                        label="Matricula"
                        value={registration}
                        onChange={handleRegistrationChange}
                    />
                </div>
            </div>
        </Grid>
    );
}

export { IdentificationPicture };
