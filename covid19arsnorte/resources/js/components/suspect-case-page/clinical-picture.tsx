import React, { ChangeEvent, useState } from "react";
import {
    FormControlLabel,
    Checkbox,
    FormGroup,
    TextField,
    Typography
} from "@material-ui/core";

import { CheckSymptom, FreeTextSymptom, Symptom, SYMPTOM_TYPE } from "./types";
import { DatePicker } from "../date-picker";

interface ClinicalPictureProps {
    symptomGroups: Symptom[][];
    disabled?: boolean;
    onSymptomChange?: (symptom: Symptom) => void;
    onSymptomDateChange?: (date: Date) => void;
}

function ClinicalPicture({
    symptomGroups,
    disabled,
    onSymptomChange,
    onSymptomDateChange
}: ClinicalPictureProps): JSX.Element {
    const [date, setDate] = useState(new Date());

    const handleDateChange = (date: Date): void => {
        setDate(date);
        onSymptomDateChange && onSymptomDateChange(date);
    };

    const SYMPTOM_STRATEGIES = {
        [SYMPTOM_TYPE.CHECK]: function createCheckSymptom(
            symptom: CheckSymptom
        ): JSX.Element {
            const handleChange = () => (
                event: ChangeEvent<HTMLInputElement>
            ): void => {
                onSymptomChange &&
                    onSymptomChange({
                        ...symptom,
                        value: event.target.checked
                    });
            };

            return (
                <FormControlLabel
                    key={symptom.id}
                    disabled={disabled}
                    control={
                        <Checkbox
                            checked={symptom.value}
                            value={symptom.id}
                            onChange={handleChange()}
                        />
                    }
                    label={symptom.name}
                />
            );
        },
        [SYMPTOM_TYPE.FREE_TEXT]: function createFreeTextSymptom(
            symptom: FreeTextSymptom
        ): JSX.Element {
            //TODO missing a debounce/throttle
            const handleChange = () => (
                event: ChangeEvent<HTMLInputElement>
            ): void => {
                onSymptomChange({ ...symptom, value: event.target.value });
            };

            return (
                <TextField
                    fullWidth
                    key={symptom.id}
                    disabled={disabled}
                    value={symptom.value}
                    label={symptom.name}
                    onChange={handleChange()}
                ></TextField>
            );
        }
    };

    const symptomGroupComponents = symptomGroups.map((symptoms, index) => {
        const symptomsComponents = symptoms.map(symptom => {
            switch (symptom.type) {
                case SYMPTOM_TYPE.CHECK:
                    return SYMPTOM_STRATEGIES[SYMPTOM_TYPE.CHECK](symptom);
                case SYMPTOM_TYPE.FREE_TEXT:
                    return SYMPTOM_STRATEGIES[SYMPTOM_TYPE.FREE_TEXT](symptom);
                default:
                    throw new Error("invalid symptom");
            }
        });

        return (
            <FormGroup key={index} row>
                {symptomsComponents}
            </FormGroup>
        );
    });

    return (
        <div>
            <h3>Dados sobre infeção:</h3>
            <DatePicker
                label="Data Inicio Sintomas"
                disabled={disabled}
                value={date}
                required
                onDateChange={handleDateChange}
                autoOk
                disableFuture
            ></DatePicker>
            <div style={{ marginTop: 30 }}>
                <h4>Quadro clínico:</h4>
                {symptomGroupComponents}
            </div>
        </div>
    );
}

export { ClinicalPicture };
