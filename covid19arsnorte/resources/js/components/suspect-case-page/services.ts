import { Entry, GLOBAL_ACTION_TYPE } from "../../types";
import { Suspect } from "./types";
import { useState, useEffect } from "react";
import { updateEntry, addEntry, searchEntry, getEntry } from "../../services";
import { useGlobalState } from "../../context";

function toSuspect(entry: Entry): Suspect {
    return {
        ...entry,
        abdominalPain: entry.abdominal_pain,
        otherSymptoms: entry.other_sympthoms,
        originalEntry: entry
    };
}

async function search(idType: string, idNumber: string): Promise<Suspect> {
    const entries = await searchEntry({ idType, idNumber });
    return entries
        .sort((entryA, entryB) => {
            return entryB.updated_at.getTime() - entryA.updated_at.getTime();
        })
        .map(toSuspect)[0];
}

async function save(suspect: Suspect): Promise<Suspect> {
    const updatedEntry: Entry = {
        ...suspect.originalEntry,
        ...suspect,
        abdominal_pain: suspect.abdominalPain,
        other_sympthoms: suspect.otherSymptoms || null
    };
    delete updatedEntry["abdominalPain"];
    delete updatedEntry["otherSymptoms"];

    if (suspect.id) {
        await updateEntry(updatedEntry);

        return suspect;
    }

    const { id: newSuspectId } = await addEntry({
        ...updatedEntry,
        has_arrived: false
    });

    return {
        ...suspect,
        id: newSuspectId
    };
}

function useSuspect(): [
    Suspect,
    (idType: string, idNumber: string) => void,
    (suspect: Suspect) => void,
    () => void,
    () => void,
    string | null,
    string | null,
    string | null,
    string | null
] {
    const [searchParams, setSearchParams] = useState<[string, string]>();
    const [suspect, setSuspect] = useState<Suspect>();
    const [targetSuspect, setTargetSuspect] = useState<Suspect>();
    const [searchError, setSearchError] = useState(null);
    const [saveError, setSaveError] = useState(null);
    const [isSaved, setIsSaved] = useState(null);
    const [isLoaded, setIsLoaded] = useState(null);

    const [, dispatch] = useGlobalState();

    const _search = (idType: string, idNumber: string): void => {
        setSearchParams([idType, idNumber]);
    };

    useEffect(() => {
        (async (): Promise<void> => {
            if (!searchParams) {
                return;
            }
            const [idType, idNumber] = searchParams;

            try {
                dispatch({ type: GLOBAL_ACTION_TYPE.START_LOADING });

                const suspect = await search(idType, idNumber);

                dispatch({ type: GLOBAL_ACTION_TYPE.STOP_LOADING });
                setSuspect(suspect);
                setIsLoaded(true);
                setSearchError(null);
            } catch (error) {
                dispatch({ type: GLOBAL_ACTION_TYPE.STOP_LOADING });
                setIsLoaded(false);
                setSearchError(error.message);
            }
        })();
    }, [searchParams]);

    useEffect(() => {
        if (!targetSuspect) {
            return;
        }

        (async (): Promise<void> => {
            try {
                dispatch({ type: GLOBAL_ACTION_TYPE.START_LOADING });
                await save(targetSuspect);
                dispatch({ type: GLOBAL_ACTION_TYPE.STOP_LOADING });
                setIsSaved(true);
                setSaveError(null);
            } catch (error) {
                dispatch({ type: GLOBAL_ACTION_TYPE.STOP_LOADING });
                setIsSaved(false);
                setSaveError(error.message);
            }
        })();
    }, [targetSuspect]);

    const updateSuspect = (suspect: Suspect): void => {
        setTargetSuspect(suspect);
    };

    const clearSuspect = (): void => {
        setSuspect(undefined);
        clearStateHandlers();
    };

    const clearStateHandlers = (): void => {
        setIsLoaded(null);
        setSearchError(null);
        setIsSaved(null);
        setSaveError(null);
    };

    return [
        suspect,
        _search,
        updateSuspect,
        clearSuspect,
        clearStateHandlers,
        isLoaded,
        searchError,
        isSaved,
        saveError
    ];
}

async function getSuspect(id: string): Promise<Suspect> {
    const entry = await getEntry(id);

    return toSuspect(entry);
}

export { useSuspect, getSuspect };
