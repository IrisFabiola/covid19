import React from "react";
import {
    FormControlLabel,
    Radio,
    RadioGroup,
    FormGroup,
    TextField
} from "@material-ui/core";

import { DatePicker } from "../date-picker";
import { RangeDatePicker } from "../range-date-picker";

interface TravelPictureProps {
    traveledToForeign: boolean | null;
    foreignCountry: string;
    startTravelDate: Date;
    returnTravelDate: Date;
    contactWithInfected: string;
    confirmedCaseName: string;
    confirmedCaseContactDate: Date;
    isHealthProfessional: boolean | null;
    disabled?: boolean;
    onTraveledToForeignChange?: (traveledToForeign: boolean) => void;
    onStartTravelDateChange?: (date: Date) => void;
    onReturnTravelDateChange?: (date: Date) => void;
    onForeignCountryChange?: (country: string) => void;
    onConfirmedCaseContactDateChange?: (date: Date) => void;
    onContactWithInfectedChange?: (contactWithInfected: string) => void;
    onConfirmedCaseNameChange?: (confirmedCaseName: string) => void;
    onIsHealthProfessionalChange?: (isHealthProfessional: boolean) => void;
}

function TravelPicture({
    traveledToForeign,
    foreignCountry,
    startTravelDate,
    returnTravelDate,
    contactWithInfected,
    confirmedCaseName,
    confirmedCaseContactDate,
    isHealthProfessional,
    disabled,
    onTraveledToForeignChange,
    onForeignCountryChange,
    onStartTravelDateChange,
    onReturnTravelDateChange,
    onContactWithInfectedChange,
    onConfirmedCaseNameChange,
    onConfirmedCaseContactDateChange,
    onIsHealthProfessionalChange
}: TravelPictureProps): JSX.Element {
    const handleTraveledToForeignChange = (event): void => {
        const traveledToForeign = event.target.value;
        onTraveledToForeignChange &&
            onTraveledToForeignChange(traveledToForeign === "YES");
    };

    const handleStartTravelDateChange = (date: Date): void => {
        onStartTravelDateChange && onStartTravelDateChange(date);
    };

    const handleReturnTravelDateChange = (date: Date): void => {
        onReturnTravelDateChange && onReturnTravelDateChange(date);
    };

    const handleCountrySelectChange = (event): void => {
        const country = event.target.value;
        onForeignCountryChange && onForeignCountryChange(country);
    };

    const handleContactWithInfectedChange = (event): void => {
        const contactWithInfected = event.target.value;
        onContactWithInfectedChange &&
            onContactWithInfectedChange(contactWithInfected);
    };

    const handleConfirmedCaseNameChange = (event): void => {
        const infectedName = event.target.value;
        onConfirmedCaseNameChange && onConfirmedCaseNameChange(infectedName);
    };

    const handleConfirmedCaseContactDateChange = (date: Date): void => {
        onConfirmedCaseContactDateChange &&
            onConfirmedCaseContactDateChange(date);
    };

    const handleHealthProfessionalChange = (event): void => {
        const isHealthProfessional = event.target.value === "YES";
        onIsHealthProfessionalChange &&
            onIsHealthProfessionalChange(isHealthProfessional);
    };

    const countrySelector = (
        <TextField
            label="País"
            disabled={disabled}
            name="recent_trips"
            value={foreignCountry}
            required
            onChange={handleCountrySelectChange}
        ></TextField>
    );

    const renderForeignSection = (): JSX.Element => (
        <div>
            {countrySelector}
            <RangeDatePicker
                startLabel="Data da ida:"
                startValue={startTravelDate}
                endLabel="Data do regresso"
                endValue={returnTravelDate}
                disabled={disabled}
                required
                autoOk
                disableFuture
                onStartDateChange={handleStartTravelDateChange}
                onEndDateChange={handleReturnTravelDateChange}
            ></RangeDatePicker>
        </div>
    );

    const renderContactFormSection = (): JSX.Element => (
        <FormGroup row>
            <div>
                <TextField
                    label="Nome"
                    name="contact_confirmed_case_name"
                    disabled={disabled}
                    value={confirmedCaseName}
                    required
                    fullWidth
                    onChange={handleConfirmedCaseNameChange}
                ></TextField>
            </div>
            <div>
                <DatePicker
                    label="Data de contacto"
                    disabled={disabled}
                    value={confirmedCaseContactDate}
                    required
                    autoOk
                    disableFuture
                    onDateChange={handleConfirmedCaseContactDateChange}
                />
            </div>
        </FormGroup>
    );

    const hasTraveledToForeign = traveledToForeign ? "YES" : traveledToForeign === null ? "UNKNOWN" : "NO"
    const healthProfessionalStatus = isHealthProfessional ? "YES" : isHealthProfessional === null ? "UNKNOWN" : "NO";

    return (
        <>
            <h2>Dados epidemiológicos:</h2>
            <FormGroup>
                <FormGroup>
                    <RadioGroup
                        aria-label="traveled to Foreign"
                        name="travel_to_foreign"
                        value={hasTraveledToForeign}
                        onChange={handleTraveledToForeignChange}
                        row
                    >
                        <label>Viagem para o estrangeiro?</label>
                        <FormControlLabel
                            value="NO"
                            disabled={disabled}
                            labelPlacement="top"
                            control={<Radio required />}
                            label="Não"
                        />
                        <FormControlLabel
                            value="YES"
                            disabled={disabled}
                            labelPlacement="top"
                            control={<Radio required />}
                            label="Sim"
                        />
                    </RadioGroup>
                </FormGroup>
                {traveledToForeign && renderForeignSection()}

                <FormGroup>
                    <RadioGroup
                        aria-label="Teve contacto com um caso confirmado de 2019-nCoV?"
                        name="contact_confirmed_case"
                        value={contactWithInfected}
                        onChange={handleContactWithInfectedChange}
                        row
                    >
                        <label>
                            Teve contacto com um caso confirmado de 2019-nCoV?
                        </label>
                        <FormControlLabel
                            value="NO"
                            disabled={disabled}
                            labelPlacement="top"
                            control={<Radio required />}
                            label="Não"
                        />
                        <FormControlLabel
                            value="YES"
                            disabled={disabled}
                            labelPlacement="top"
                            control={<Radio required />}
                            label="Sim"
                        />
                        <FormControlLabel
                            value="UNKNOWN"
                            disabled={disabled}
                            labelPlacement="top"
                            control={<Radio required />}
                            label="Desconhecido"
                        />
                    </RadioGroup>
                    {contactWithInfected === "YES" &&
                        renderContactFormSection()}
                </FormGroup>

                <FormGroup>
                    <RadioGroup
                        aria-label="O doente é profissional de saúde??"
                        name="is_health_profissional"
                        value={healthProfessionalStatus}
                        onChange={handleHealthProfessionalChange}
                        row
                    >
                        <label>O doente é profissional de saúde?</label>
                        <FormControlLabel
                            value="NO"
                            disabled={disabled}
                            labelPlacement="top"
                            control={<Radio required />}
                            label="Não"
                        />
                        <FormControlLabel
                            value="YES"
                            disabled={disabled}
                            labelPlacement="top"
                            control={<Radio required />}
                            label="Sim"
                        />
                    </RadioGroup>
                </FormGroup>
            </FormGroup>
        </>
    );
}

export { TravelPicture };
