import { timer, from, Observable } from "rxjs";
import { switchMap, share, map } from "rxjs/operators";

import { DOCUMENT_TYPES, Entry, GLOBAL_ACTION_TYPE } from "../../types";
import { PeopleMap, Person } from "./types";
import { useState, useEffect, useRef } from "react";
import { getAllEntries, updateEntry, searchEntry } from "../../services";
import { useGlobalState } from "../../context";

const POLLER_INTERVAL = 15000;

function toPerson(entry: Entry): Person {
    return {
        key: entry.id,
        id: entry.id_number,
        name: entry.name,
        documentType: entry.id_type as DOCUMENT_TYPES,
        hasArrived: entry.has_arrived,
        hasContractedDisease: entry.is_positive,
        registration: entry.registration,
        createdDate: entry.created_at,
        updatedDate: entry.updated_at,
        originalEntry: entry
    };
}

async function getPeople(
    page: number,
    size: number
): Promise<{ peopleMap: PeopleMap; hasReachedEnd: boolean }> {
    const fetchedEntries = await getAllEntries(page, size);
    const entries = fetchedEntries.data
        .map(toPerson)
        .map(person => [person.key, person]);

    return {
        peopleMap: Object.fromEntries(entries),
        hasReachedEnd: fetchedEntries.hasReachedEnd
    };
}

async function save(person: Person): Promise<Person> {
    const updatedEntry: Entry = {
        ...person.originalEntry,
        has_arrived: person.hasArrived,
        is_positive: person.hasContractedDisease,
        registration: person.registration
    };

    await updateEntry(updatedEntry);

    return person;
}

function peopleStream(size: number): Observable<PeopleMap> {
    return timer(0, POLLER_INTERVAL).pipe(
        switchMap(() => from(getPeople(1, size))),
        map(({ peopleMap }) => peopleMap),
        share()
    );
}

function usePeople(
    page: number,
    size: number
): [Person, (person: Person) => void] {
    const [, dispatch] = useGlobalState();
    const lastPromise = useRef<Promise<Person>>();
    const [updatedPerson, setUpdatedPerson] = useState<Person>();

    const updatePerson = (person: Person): void => {
        setUpdatedPerson(person);
    };

    useEffect((): void => {
        if (!updatedPerson) {
            return;
        }

        (async (): Promise<void> => {
            dispatch({ type: GLOBAL_ACTION_TYPE.START_LOADING });

            const currentPromise = save(updatedPerson);
            lastPromise.current = currentPromise;

            const savedPerson = await currentPromise;

            if (currentPromise !== lastPromise.current) {
                return;
            }

            //update with server info
            dispatch({ type: GLOBAL_ACTION_TYPE.STOP_LOADING });
            dispatch({
                type: GLOBAL_ACTION_TYPE.UPDATE_PERSON,
                payload: savedPerson
            });
        })();
    }, [updatedPerson]);

    useEffect(() => {
        const subscription = peopleStream(size).subscribe(newPeopleMap => {
            dispatch({
                type: GLOBAL_ACTION_TYPE.UPDATE_PEOPLE_MAP,
                payload: newPeopleMap
            });
        });

        return (): void => {
            subscription.unsubscribe();
        };
    }, [size]);

    return [updatedPerson, updatePerson];
}

interface SearchParams {
    idType?: string;
    idNumber?: string;
    registration?: string;
}
async function search({
    idType,
    idNumber,
    registration
}: SearchParams): Promise<Person[]> {
    const entries = await searchEntry({ idType, idNumber, registration });

    return entries.map(toPerson);
}

export { getPeople, usePeople, save, search };
