import React, {
    createContext,
    useContext,
    useReducer,
    Reducer,
    Dispatch
} from "react";

import { GlobalState, GlobalAction } from "./types";

interface GlobalStateProviderProps {
    reducer: Reducer<GlobalState, GlobalAction>;
    initialState?: GlobalState;
    children?: React.ReactNode;
}

const initial: GlobalState = {
    isLoadingCounter: 0,
    peopleMap: {}
};

export const StateContext = createContext<
    [GlobalState, Dispatch<GlobalAction>]
>([
    initial,
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    (): void => {}
]);

export const GlobalStateProvider = ({
    reducer,
    initialState = initial,
    children
}: GlobalStateProviderProps): JSX.Element => {
    return (
        <StateContext.Provider value={useReducer(reducer, initialState)}>
            {children}
        </StateContext.Provider>
    );
};

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const useGlobalState = () => useContext(StateContext);
