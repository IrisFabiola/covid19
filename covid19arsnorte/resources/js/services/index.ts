import { ServerEntry, Entry } from "../types";
import { format } from "date-fns";

const endpoint = window._env.API_ENDPOINT;

const SERVER_APP_RENAMES = {
    is_approved: "approved"
};
const APP_SERVER_RENAMES = Object.fromEntries(
    Object.entries(SERVER_APP_RENAMES).map(([key, value]) => [value, key])
);

const SHORT_DATE_FIELDS = [
    "birth_date",
    "date_first_symptoms",
    "contact_confirmed_case_date",
    "trip_return_portugal"
];
const FULL_DATE_FIELDS = ["created_at", "updated_at"];
const BOOLEAN_FIELDS = [
    "fever",
    "cough",
    "chills",
    "otophagia",
    "coryza",
    "headaches",
    "myalgia",
    "dyspnoea",
    "abdominal_pain",
    "vomits",
    "diarrhea",
    "is_health_profissional",
    "is_approved"
];
const STRING_FIELDS = ["is_positive", "has_arrived"];

function buildUrl(path: string): URL {
    return new URL(`${endpoint}${path}`, window.location.toString());
}

async function fetchFromServer<T>(
    url: URL,
    fetchParams?: RequestInit
): Promise<T> {
    const defaultParams: RequestInit = {
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    };

    const params = {
        ...defaultParams,
        ...fetchParams
    };
    const response = await fetch(url.toString(), params);

    if (!response.ok) {
        throw new Error(response.statusText);
    }

    return response.json();
}

function toEntry(dataEntry: ServerEntry): Entry {
    return Object.fromEntries(
        Object.entries(dataEntry).map(([serverKey, value]) => {
            const key = APP_SERVER_RENAMES[serverKey] || serverKey;

            if (SHORT_DATE_FIELDS.includes(key)) {
                return [key, new Date(value)];
            }

            if (BOOLEAN_FIELDS.includes(key)) {
                return [key, value === "YES"];
            }

            if (FULL_DATE_FIELDS.includes(key)) {
                return [key, new Date(value)];
            }

            if (STRING_FIELDS.includes(key)) {
                return [key, value === "UNKNOWN" ? null : value === "YES"];
            }

            if (key === "other_sympthoms" && value === null) {
                return [key, ""];
            }

            return [key, value];
        })
    ) as Entry;
}

function toServerEntry(entry: Partial<Entry>): ServerEntry {
    const datesAsStrings: Partial<ServerEntry> = Object.fromEntries(
        SHORT_DATE_FIELDS.filter(field => entry[field]).map(field => {
            const date: Date = entry[field];

            return [field, format(date, "yyyy-MM-dd")];
        })
    );
    const booleansAsStrings: Partial<ServerEntry> = Object.fromEntries([
        ...BOOLEAN_FIELDS.map(field => [field, entry[field] ? "YES" : "NO"]),
        ...STRING_FIELDS.map(field => {
            if ((entry[field] ?? "UNKNOWN") === "UNKNOWN") {
                return [field, "UNKNOWN"];
            }

            return [field, entry[field] ? "YES" : "NO"];
        })
    ]);
    const parsedEntry = {
        ...entry,
        ...datesAsStrings,
        ...booleansAsStrings
    };

    const serverEntry = Object.fromEntries(
        Object.entries(parsedEntry).map(([key, value]) => {
            const parsedKey = SERVER_APP_RENAMES[key] || key;

            return [parsedKey, value];
        })
    );

    return serverEntry as ServerEntry;
}

async function updateEntry(entry: Partial<Entry>): Promise<void> {
    const serverEntry = toServerEntry(entry);

    await fetchFromServer(buildUrl(`/entries/${entry.id}`), {
        method: "PUT",
        body: JSON.stringify(serverEntry)
    });
}

async function addEntry(entry: Entry): Promise<Entry> {
    const serverEntry = toServerEntry(entry);

    const id = await fetchFromServer<string>(buildUrl("/entries"), {
        method: "POST",
        body: JSON.stringify(serverEntry)
    });

    return {
        ...entry,
        id
    };
}

interface SearchParams {
    idType?: string;
    idNumber?: string;
    registration?: string;
}
async function searchEntry({
    idType,
    idNumber,
    registration
}: SearchParams): Promise<Entry[]> {
    interface ServerError {
        error: string;
    }

    const url = buildUrl("/entries/search");
    url.searchParams.append("id_type", idType);
    url.searchParams.append("id_number", idNumber);
    url.searchParams.append("registration", registration);

    const serverEntries = await fetchFromServer<ServerEntry[] | ServerError>(
        url
    );

    const isError = (x: ServerEntry[] | ServerError): x is ServerError =>
        !!(x as ServerError).error;

    if (isError(serverEntries)) {
        throw new Error(serverEntries.error);
    }

    if (serverEntries.length === 0) {
        throw new Error("Pessoa não encontrada na base de dados.");
    }

    return serverEntries.map(toEntry);
}

interface PaginatedEntryResponse {
    data: Entry[];
    hasReachedEnd: boolean;
}

async function getAllEntries(
    page: number,
    size: number
): Promise<PaginatedEntryResponse> {
    interface Response {
        data: ServerEntry[];
        next_page_url: string;
    }

    const url = buildUrl("/entries");
    url.searchParams.append("page", "" + page);
    url.searchParams.append("size", "" + size);

    const response = await fetchFromServer<Response>(url);

    return {
        data: response.data.map(toEntry),
        hasReachedEnd: response.next_page_url === null
    };
}

async function getEntry(id: string): Promise<Entry> {
    const url = buildUrl(`/entries/${id}`);

    const response = await fetchFromServer<ServerEntry>(url);

    return toEntry(response);
}

export { addEntry, updateEntry, searchEntry, getAllEntries, toEntry, getEntry };
