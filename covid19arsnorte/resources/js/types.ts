import { PeopleMap, Person } from "./components/people-list/types";

export enum DOCUMENT_TYPES {
    PASSPORT = "PASSPORT",
    CITIZEN_CARD = "CITIZEN_CARD",
    SNS = "SNS",
    DRIVERS_LICENSE = "DRIVERS_LICENSE"
}

export interface ServerEntry {
    id: string;
    is_positive: string;
    id_number: string;
    id_type: string;
    name: string;
    birth_date: string;
    gender: string;
    contact: string;
    address: string;
    registration: string;
    date_first_symptoms: string;
    fever: string;
    cough: string;
    chills: string;
    otophagia: string;
    coryza: string;
    headaches: string;
    myalgia: string;
    dyspnoea: string;
    abdominal_pain: string;
    vomits: string;
    diarrhea: string;
    other_sympthoms: string;
    outside_country_residence: string;
    location: string;
    friend_contact: string;
    recent_trips: string;
    trip_departure: string;
    trip_return: string;
    trip_return_portugal: string;
    contact_confirmed_case: string;
    contact_confirmed_case_name: string;
    contact_confirmed_case_date: string;
    is_health_profissional: string;
    approved: string;
    created_at: string;
    updated_at: string;
}

export interface Entry {
    id: string;
    is_positive: boolean | null;
    id_number: string;
    id_type: DOCUMENT_TYPES;
    name: string;
    birth_date: Date;
    gender: string;
    contact: string;
    address: string;
    registration: string;
    date_first_symptoms: Date;
    fever: boolean;
    cough: boolean;
    chills: boolean;
    otophagia: boolean;
    coryza: boolean;
    headaches: boolean;
    myalgia: boolean;
    dyspnoea: boolean;
    abdominal_pain: boolean;
    vomits: boolean;
    diarrhea: boolean;
    other_sympthoms: string;
    outside_country_residence: string;
    location: string;
    friend_contact: string;
    recent_trips: string;
    trip_departure: string;
    trip_return: string;
    trip_return_portugal: Date;
    contact_confirmed_case: string;
    contact_confirmed_case_name: string;
    contact_confirmed_case_date: Date;
    is_health_profissional: boolean;
    has_arrived: boolean | null;
    is_approved: boolean;
    created_at: Date;
    updated_at: Date;
    nationality: string;
    profession: string;
    email: string;
}

export interface GlobalState {
    isLoadingCounter: number;
    peopleMap: PeopleMap;
}

export enum GLOBAL_ACTION_TYPE {
    START_LOADING,
    STOP_LOADING,
    UPDATE_PEOPLE_MAP,
    UPDATE_PERSON
}

export interface StartLoadingAction {
    type: GLOBAL_ACTION_TYPE.START_LOADING;
}

export interface StopLoadingAction {
    type: GLOBAL_ACTION_TYPE.STOP_LOADING;
}

export interface UpdatePeopleMap {
    type: GLOBAL_ACTION_TYPE.UPDATE_PEOPLE_MAP;
    payload: PeopleMap;
}

export interface UpdatePerson {
    type: GLOBAL_ACTION_TYPE.UPDATE_PERSON;
    payload: Person;
}

export type GlobalAction =
    | StartLoadingAction
    | StopLoadingAction
    | UpdatePeopleMap
    | UpdatePerson;

declare global {
    interface Window {
        _env: { API_ENDPOINT: string };
    }
}
