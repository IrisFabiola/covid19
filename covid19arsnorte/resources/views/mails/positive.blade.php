<p>Análise positiva registada</p>

<div>
<p><b>Nome:</b>&nbsp;{{ $entry->name }}</p>
<p><b>Contacto:</b>&nbsp;{{ $entry->contact }}</p>
<p><b>Contacto do acompanhante:</b>&nbsp;{{ $entry->friend_contact }}</p>
<p><b>Morada:</b>&nbsp;{{ $entry->address }}</p>
<p><b>Local do caso:</b>&nbsp;{{ $entry->location }}</p>
</div>

<p>
    <a href="{{ env('APP_URL') . 'person/' .$entry->id }}">Ver mais</a>
</p>
