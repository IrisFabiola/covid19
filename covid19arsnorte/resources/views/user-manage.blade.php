@extends('layouts.app')

@section('head')
<script src="{{ mix('js/app.js') }}" defer></script>
<style>
    table {
        width: calc(100% - 40px);
        border-collapse: collapse;
        border-spacing: 0;
        empty-cells: show;
        border: 1px solid #cbcbcb;
        margin: 20px;
    }

    table td, table th {
        border-left: 1px solid #cbcbcb;
        border-width: 0 0 0 1px;
        font-size: inherit;
        margin: 0;
        overflow: visible;
        padding: .5em 1em
    }

    table thead {
        background-color: #e0e0e0;
        color: #000;
        text-align: left;
        vertical-align: bottom
    }

    table td {
        background-color: transparent
    }

    tr:nth-child(2n-1) td {
        background-color: #f2f2f2
    }

    td {
        border-bottom: 1px solid #cbcbcb
    }

    tbody > tr:last-child > td {
        border-bottom-width: 0
    }
</style>
@endsection

@section('content')
    @isset($message)
        <script>
            if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
            }
        </script>
        <div class="alert alert-success alert-dismissible" style="text-align: center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>{{ $message }}</strong>
        </div>
    @endisset
    <table>
        <thead>
        <tr>
            <th>Nome</th>
            <th>Email</th>
            <th>Tipo de Acesso</th>
            <th>Passar a:</th>
        </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->userType() }}</td>
                <td>
                    <div>
                        <form action="{{ route('manage-roles') }}" method="POST">
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <input id="type-{{ $user->id }}"type="hidden" name="type">
                        @csrf
                        @foreach($user->otherUserTypes() as $userType)
                            <Button onclick="document.querySelector('#type-{{ $user->id }}').value='{{ $userType->value }}'" type="submit">
                                {{ $userType->label }}
                            </Button>
                        @endforeach
                        </form>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
