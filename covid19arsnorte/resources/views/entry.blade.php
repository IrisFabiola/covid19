@extends('layouts.app')

@section('head')
<style>
    table {
        width: calc(100% - 40px);
        border-collapse: collapse;
        border-spacing: 0;
        empty-cells: show;
        border: 1px solid #cbcbcb;
        margin: 20px;
    }

    table td, table th {
        border-left: 1px solid #cbcbcb;
        border-width: 0 0 0 1px;
        font-size: inherit;
        margin: 0;
        overflow: visible;
        padding: .5em 1em
    }

    table thead {
        background-color: #e0e0e0;
        color: #000;
        text-align: left;
        vertical-align: bottom
    }

    table td {
        background-color: transparent
    }

    tr:nth-child(2n-1) td {
        background-color: #f2f2f2
    }

    td {
        border-bottom: 1px solid #cbcbcb
    }

    tbody > tr:last-child > td {
        border-bottom-width: 0
    }
</style>
@endsection

@section('content')
    @if($entry)
    <table>
        <tr>
            <th>Nome</th>
            <td>{{ $entry->name }}</td>
        </tr>
        <tr>
            <th>Resultado Análise</th>
            <td>
                @if($entry->is_positive == 'YES')
                    Positivo
                @elseif($entry->is_positive == 'NO')
                    Negativo
                @endif
            </td>
        </tr>
        <tr>
            <th>Data de nascimento</th>
            <td>{{ $entry->birth_date }}</td>
        </tr>
        <tr>
            <th>Sexo</th>
            <td>{{ $entry->gender }}</td>
        </tr>
        <tr>
            <th>
            @switch($entry->id_type)
                @case('SNS')
                    Número Utente / SNS
                    @break
                @case('PASSPORT')
                    Passaporte
                    @break
                @case('CITIZEN_CARD')
                    Cartão Cidadão
                    @break
                @case('DRIVERS_LICENSE')
                    Carta Condução
                    @break
                @default
                    Número de Identificação
            @endswitch
            </th>
            <td>{{ $entry->id_number }}</td>
        </tr>
        <tr>
            <th>Contacto</th>
            <td>{{ $entry->contact }}</td>
        </tr>
        <tr>
            <th>Morada</th>
            <td>{{ $entry->address }}</td>
        </tr>
        <tr>
            <th>Matrícula</th>
            <td>{{ $entry->registration }}</td>
        </tr>
        <tr>
            <th>Nacionalidade</th>
            <td>{{ $entry->nationality }}</td>
        </tr>
        <tr>
            <th>Profissão</th>
            <td>{{ $entry->profession }}</td>
        </tr>
        <tr>
            <th>E-mail</th>
            <td>{{ $entry->email }}</td>
        </tr>
        <tr>
            <th>Data Início Sintomas</th>
            <td>{{ $entry->date_first_symptoms }}</td>
        </tr>
        @if(!is_null($entry->outside_country_residence))
        <tr>
            <th>País de Residência</th>
            <td>{{ $entry->outside_country_residence }}</td>
        </tr>
        @endif
        <tr>
            <th>Local do Caso</th>
            <td>{{ $entry->location }}</td>
        </tr>
        <tr>
            <th>Contacto Acompanhante</th>
            <td>{{ $entry->friend_contact }}</td>
        </tr>
        <tr>
            <th>Viagens Estrangeiro</th>
            <td>
                @if($entry->recent_trips == 'YES')
                    Sim
                @elseif($entry->recent_trips == 'NO')
                    Não
                @endif
            </td>
        </tr>
        @if($entry->recent_trips)
        <tr>
            <th>Data da Ida</th>
            <td>{{ $entry->trip_departure }}</td>
        </tr>
        <tr>
            <th>Data do Regresso</th>
            <td>{{ $entry->trip_return }}</td>
        </tr>
        @endif
        <tr>
            <th>Contacto com caso confirmado de COVID-19</th>
            <td>
                @if($entry->contact_confirmed_case == 'YES')
                    Sim
                @elseif($entry->contact_confirmed_case == 'NO')
                    Não
                @endif
            </td>
        </tr>
        @if($entry->contact_confirmed_case == 'YES')
        <tr>
            <th>Nome</th>
            <td>{{ $entry->contact_confirmed_case_name }}</td>
        </tr>
        <tr>
            <th>Data</th>
            <td>{{ $entry->contact_confirmed_case_date }}</td>
        </tr>
        @endif
        <tr>
            <th>É profissional de saúde</th>
            <td>
                @if($entry->is_health_profissional == 'YES')
                    Sim
                @elseif($entry->is_health_profissional == 'NO')
                    Não
                @endif
            </td>
        </tr>
        <tr>
            <th>Local de Registo</th>
            <td>{{ $entry->registered_on }}</td>
        </tr>
        <tr>
            <th>Sintomas</th>
            <td>
                <ul>
                    @if($entry->fever == 'YES')
                        <li>Febre</li>
                    @endif
                    @if($entry->cough == 'YES')
                        <li>Tosse</li>
                    @endif
                    @if($entry->chills == 'YES')
                        <li>Calafrios</li>
                    @endif
                    @if($entry->otophagia == 'YES')
                        <li>Odinofagia</li>
                    @endif
                    @if($entry->coryza == 'YES')
                        <li>Coriza</li>
                    @endif
                    @if($entry->headaches == 'YES')
                        <li>Cefaleia</li>
                    @endif
                    @if($entry->myalgia == 'YES')
                        <li>Mialgia</li>
                    @endif
                    @if($entry->dyspnoea == 'YES')
                        <li>Dispneia</li>
                    @endif
                    @if($entry->abdominal_pain == 'YES')
                        <li>Dor abdominal</li>
                    @endif
                    @if($entry->vomits == 'YES')
                        <li>Vomitos</li>
                    @endif
                    @if($entry->diarrhea == 'YES')
                        <li>Diarreia</li>
                    @endif
                    @if($entry->other_sympthoms == 'YES')
                        <li>{{ $entry->other_sympthoms }}</li>
                    @endif
                </ul>
            </td>
        </tr>
        <tr>
            <th>Registo criado em</th>
            <td>{{ $entry->created_at }}</td>
        </tr>
        @if($entry->updated_at != $entry->created_at)
        <tr>
            <th>Registo actualizado em</th>
            <td>{{ $entry->updated_at }}</td>
        </tr>
        @endif
    </table>
    @endif
@endsection
