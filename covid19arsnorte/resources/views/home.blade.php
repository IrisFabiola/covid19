@extends('layouts.app')

@section('head')
<script src="{{ mix('js/app.js') }}" defer></script>
@endsection
@section('content')
    <div id="app"></div>
@endsection