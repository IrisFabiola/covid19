<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject as AuthenticatableUserContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_type', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin() {
        return $this->user_type === 'ADMIN';
    }

    public function isLabUser() {
        return $this->user_type === 'LAB';
    }

    private function transformUserType($userType) {
        if ($userType === 'ADMIN') {
            return 'Administrador';
        }

        if ($userType === 'HEADQUARTERS') {
            return 'Centro de distribuição';
        }

        if ($userType === 'LAB') {
            return 'Laboratório';
        }
    }

    public function userType() {
        return $this->transformUserType($this->user_type);
    }

    public function otherUserTypes() {
        return collect([
            'ADMIN', 'HEADQUARTERS', 'LAB'
        ])
        ->filter(function($type) {
           return $type !== $this->user_type;
        })->map(function($type) {
            return (object)[
                'label' => $this->transformUserType($type),
                'value' => $type
            ];
        });
    }
}
