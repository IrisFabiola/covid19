<?php

namespace App;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot() {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Uuid::uuid4());
        });
    }

    protected $fillable = [
        'name',
        'birth_date',
        'gender',
        'id_number',
        'id_type',
        'contact',
        'address',
        'registration',
        'nationality',
        'profession',
        'email',
        'date_first_symptoms',
        'fever',
        'cough',
        'chills',
        'otophagia',
        'coryza',
        'headaches',
        'myalgia',
        'dyspnoea',
        'abdominal_pain',
        'vomits',
        'diarrhea',
        'other_sympthoms',
        'outside_country_residence',
        'location',
        'friend_contact',
        'recent_trips',
        'trip_departure',
        'trip_return',
        'trip_return_portugal',
        'contact_confirmed_case',
        'contact_confirmed_case_name',
        'contact_confirmed_case_date',
        'is_health_profissional',
        'has_arrived',
        'is_positive',
        'approved',
        'registered_on'
    ];

    public $rules = [
        'id_number'=>'required',
        'id_type'=>'required'
    ];
}
