<?php

namespace App\Http\Middleware;

use Closure;
use App\Providers\RouteServiceProvider;

class BlockLab
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->user_type === 'LAB') {
            return redirect('/people-list');
        }

        return $next($request);
    }
}
