<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	protected $table="countries";
	const CREATED_AT = null;
	const UPDATED_AT = null;
}
