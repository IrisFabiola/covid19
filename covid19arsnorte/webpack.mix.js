// eslint-ignore-file
const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.ts('resources/js/components/App.tsx', 'resources/js/components/App.js')
   .react('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')

if (!mix.inProduction()) {
   mix.sourceMaps()
      .browserSync('localhost:8080');
}

if (mix.inProduction()) {
   mix.version();
   mix.options( {
      terser: {
         terserOptions: {
            ie8: false,
            ecma: 5,
            warnings: true,
            mangle: true,
            compress: {
                dead_code: true, // eslint-disable-line camelcase
                drop_debugger: true, // eslint-disable-line camelcase
                conditionals: true,
                comparisons: true,
                evaluate: true,
                booleans: true,
                loops: true,
                unused: true,
                hoist_funs: true, // eslint-disable-line camelcase
                if_return: true, // eslint-disable-line camelcase
                inline: true,
                reduce_vars: true, // eslint-disable-line camelcase
                drop_console: true, // eslint-disable-line camelcase
                passes: 3,
                keep_infinity: true, // eslint-disable-line camelcase
            },
            output: {
                comments: false
            }
        },
        parallel: true,
        extractComments: true,
        sourceMap: true
      }
   });
}
