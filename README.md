# COVID-19 Drive-Through
Este projecto começou com o contacto feito pela ARS Norte para criar uma aplicação que ajudasse a registar doentes
 com suspeitas de COVID-19 e enviá-los para um laboratório de testes em modo drive-through.

Este projecto serviria não só para simplificar o processo de registo de doentes mas também para auxiliar na
centralização dos dados.

O projecto, estando pronto, acabou por ser cancelado por questões burocráticas, não relacionadas com questões técnicas
ou de segurança de dados.
Gostariamos de torná-lo open source para que quem quiser contribuir o possa fazer. Também gostarariamos de mencionar
que, se alguma entidade o quiser utilizar, estaremos disponíveis a instalá-lo e dar a manutenção necessária.

As principais funcionalidades da aplicação podem ser vistas abaixo. Esta contem uma janela para registar os doentes
suspeitos de COVID-19.

![Image of Form](images/teste_covid19_formulario.png)

E outra para registar que o doente já chegou ou não ao laboratório em modo de drive-through e se o resultado do seu
teste foi negativo ou positivo.

![Image of Search](images/teste_covid19_pesquisa.png)

Toda a informação é então guardada numa base de dados de forma centralizada.

# Development instructions

- Go to ./covid19arsnorte
- Run `npm ci`
- While it's downloading add the following to your .env file
    ```
        DB_CONNECTION=sqlsrv
        DB_HOST=db
        DB_PORT=1433
        DB_DATABASE=local
        DB_USERNAME=heyo
        DB_PASSWORD=n01cE5tuffZ
    ```
- Run `npm start`
- On first load (or when the db changes structure), after the server started, run `npm run migrate`
- Open `localhost:8080`

Congrats, you're running the application! But, how can you register the first user? 🔨time!

- Go to `covid19arsnorte/resources/views/layouts/app.blade.php` and add

```html
    @guest
        <li class="nav-item" style="margin-right: 40px">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        <div>
            <a class="dropdown-item" href="{{ route('register') }}">
                Adicionar Utilizador
            </a>
        </div>
    @else
```

- Go to `covid19arsnorte/app/Http/Middleware/AdminCheckMvc.php` and comment out this:

```php
    // if ($request->user()->user_type == 'ADMIN')
    // {
        return $next($request);
    // }
```

- Register a user through the application (as an admin)
- Undo all above changes
- You're ready to go! 🎉

## Contributors (in alphabetical order):
- Daniela Ferreira
- Gonçalo Matos
- Íris Carvalho
- João Faria
- João Gomes
- José Marques
- José Queirós
- Luís Azevedo
- Pedro Nogueira
- Rafael Ramalho
- Rafaela Faria
- Rui Costa
- Tiago Morais
